import graph;
import settings;
import patterns;
outformat="pdf";

//size(4mm*16,66mm);
unitsize(50);
pen thin = black + roundcap + 0.4pt;
pen minpen = black + roundcap + 0.15pt + fontsize(7pt);
pen dotpen = black + roundcap + 3pt;
defaultpen(fontsize(10pt));

real y0=0;
real y1=0.5;
real dy=0.8;
real dyr=0.1;
real ty=-1.2;

real x=0;
real z=1;
for(int i=0;i<3;++i)
{
draw((x,dy-dyr)--(x,dy+dyr),thin);
label("0/1",(x+1.25*z/2,dy));
draw((x,y0)--(x,y1)--(x+0.4*z,y1)--(x+0.4*z,y0)--(x+(i==1?1.6:1.2)*z,y0),thin);
draw((x,y1)--(x+0.8*z,y1)--(x+0.8*z,y0),thin);
x+=(i==1?1.6:1.2)*z;
draw((x,dy-dyr)--(x,dy+dyr),thin);
}
draw((x,dy-dyr)--(x,dy+dyr),thin);
label("Konec signálu",(x+2*z/2,dy));
draw((x,y0)--(x+1.2*z,y0),thin);
label("…",(x+3*z/2,y0));

x=0;
real sy0=-1;
real sy1=-1+0.5;
draw((x,sy1)--(x,sy0)--(x+z*0.4,sy0),thin);
for(int i=0;i<8;++i)
{
	x+=0.4*z;
	draw((x,sy1)--(x,sy0)--(x+z*0.4,sy0)--(x+z*0.4,sy1)--cycle,thin);
}
x+=0.4*z;
draw((x,sy1)--(x+z*0.4,sy1),thin);
x+=0.4*z;
draw((x,sy1)--(x+1.2*z,sy1),thin);
label("…",(x+3*z/2,sy1));

x=0;
draw((x,ty-0.15-dyr)--(x,ty+dyr),thin);
for(int i=0;i<10;++i)
{
	label("\def\ms{{\rm \mu s}}$0.4$\\$\ms$",(x+(0.4/2)*z,i%2==0?ty:ty-0.15),fontsize(8pt));
	x+=0.4*z;
}
draw((x,ty-0.15-dyr)--(x,ty+dyr),thin);
