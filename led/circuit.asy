
import graph;
import settings;
import patterns;
outformat="pdf";

//size(4mm*16,66mm);
unitsize(20);
pen thin = black + roundcap + 0.4pt;
pen minpen = black + roundcap + 0.15pt + fontsize(7pt);
pen dotpen = black + roundcap + 3pt;
defaultpen(fontsize(8pt));

//draw((0,0)--(10,10));
void c(real x,real y)
{
	draw((x,y)--(x+1,y),thin);
	draw((x+0.4,y)--(x+0.4,y+1),thin);
	draw((x+0.6,y)--(x+0.6,y+1),thin);
	draw((x,y+1)--(x+1,y+1),thin);
}

void d(real x,real y)
{
	dot((x,y),dotpen);
	//filldraw(circle((y,x),0.pt));

}


real zsize = 0.1;
void zdirka(real x,real y,string ch,bool end=false)
{
	filldraw(circle((x,y),0.1),white,thin);
	label(ch,(x,y),1.5*(end?E:W),minpen);
}
void prepinac(real x,real y,int smer=1)
{
	draw((x-1.4,y-0.5)--(x-0.8,y-0.5),thin);
	draw((x-1.4,y+0.5)--(x-0.8,y+0.5),thin);
	draw((x,y)--(x-0.82,y+smer*0.38),thin);
	zdirka(x-0.8,y-0.5,"");
	zdirka(x-0.8,y+0.5,"");
	zdirka(x-1.4,y-0.5,"$-$");
	zdirka(x-1.4,y+0.5,"$+$");
	zdirka(x,y,"");
	
}
void hand(real x,real y)
{
	filldraw((x-0.6,y-0.3)--(x-0.3,y-0.3)..(x,y)..(x-0.3,y+0.3)--(x-0.6,y+0.3)--cycle,white,thin);
}
void vand(real x,real y)
{
	filldraw((x-0.5,y-1)--(x-0.5,y-0.5)..(x,y)..(x+0.5,y-0.5)--(x+0.5,y-1)--cycle,white,thin);
}
void hor(real x,real y)
{
	filldraw((x-1.3,y-0.5)--(x-1,y-0.5)--(x-1,y-0.5)..(x-0.05,y-0.05)..(x,y)--(x,y)..(x-0.05,y+0.05)..(x-1,y+0.5)--(x-1,y+0.5)--(x-1.3,y+0.5)..(x-1,y)..cycle,white,thin);
}
void vor(real x,real y)
{
	filldraw((x-0.5,y-1.3)--(x-0.5,y-1)--(x-0.5,y-1)..(x-0.05,y-0.05)..(x,y)--(x,y)..(x+0.05,y-0.05)..(x+0.5,y-1)--(x+0.5,y-1)--(x+0.5,y-1.3)..(x,y-1)..cycle,white,thin);
}
void hnor(real x,real y)
{
	filldraw((x-1.3,y-0.5)--(x-1-zsize,y-0.5)..(x-0.05-zsize,y-0.05)..(x-zsize,y)--(x-zsize,y)..(x-0.05-zsize,y+0.05)..(x-1-zsize,y+0.5)--(x-1.3,y+0.5)..(x-1,y)..cycle,white,thin);
	zdirka(x,y,"");
}
void vnor(real x,real y)
{
	filldraw((x-0.5,y-1.3)--(x-0.5,y-1-zsize)..(x-0.05,y-0.05-zsize)..(x,y-zsize)--(x,y-zsize)..(x+0.05,y-0.05-zsize)..(x+0.5,y-1-zsize)--(x+0.5,y-1.3)..(x,y-1)..cycle,white,thin);
	zdirka(x,y,"");
}

void hneg(real x,real y)
{
	filldraw((x-zsize,y)--(x-0.5-zsize,y-0.3)--(x-0.5-zsize,y+0.3)--cycle,white,thin);
	zdirka(x,y,"");
}
void hrneg(real x,real y)
{
	filldraw((x+zsize,y)--(x+0.5+zsize,y-0.3)--(x+0.5+zsize,y+0.3)--cycle,white,thin);
	zdirka(x,y,"");
}
void vneg(real x,real y)
{
	filldraw((x,y-zsize)--(x-0.3,y-0.5-zsize)--(x+0.3,y-0.5-zsize)--cycle,white,thin);
	zdirka(x,y,"");
}
void led(real x,real y)
{
	real z=0.5;
	filldraw((x,y)--(x-0.5*z,y-0.3*z)--(x-0.5*z,y+0.3*z)--cycle,white,thin);
	filldraw((x,y+0.3*z)--(x,y-0.3*z)--cycle,white,thin);
	draw((x-0.2*z,y+0.3*z)--(x-0.05*z,y+0.5*z),thin,EndArrow);
	draw((x-0.35*z,y+0.35*z)--(x-0.2*z,y+0.55*z),thin,EndArrow);
}
void diod(real x,real y)
{
	filldraw((x,y)--(x-0.5,y-0.3)--(x-0.5,y+0.3)--cycle,white,thin);
	filldraw((x,y+0.3)--(x,y-0.3)--cycle,white,thin);
}

string FALSE = "0";
string TRUE  = "1";



void innode(real x, real y)
{
	draw((x-1-0.25,y-3)--(x-1-0.25,y-5),thin);
	draw((x-1+0.25,y-3)--(x-1+0.25,y-5),thin);
	draw((x+1+0.25,y-3)--(x+1+0.25,y-5),thin);
	draw((x-2-0.25,y-1)--(x-2-0.25,y-4.25)--(x+1-0.25,y-4.25)--(x+1-0.25,y-5),thin);
	draw((x-2+0.25,y-1)--(x-2+0.25,y-4+.25)--(x-1-0.25,y-4+.25),thin);
	draw((x+1-0.25,y-3)--(x+1-0.25,y-3.75)--(x-1-0.25,y-3.75),thin);
	draw((x-0.25,y-1)--(x-0.25,y-1.75)--(x-1,y-1.75)--(x-1,y-2),thin);
	draw((x+0.25,y-1)--(x+0.25,y-1.75)--(x+1,y-1.75)--(x+1,y-2),thin);
	draw((x,y)--(x,y+0.5),thin);
	draw((x-2,y)--(x-2,y+0.5),thin);
	vor(x,y);
	vor(x-2,y);
	vand(x-1,y-2);
	vand(x+1,y-2);
	hneg(x+0.3,y-3.75);
	d(x-1-0.25,y-3.75);
}
void himp(real x,real y,real s)
{
	label("$\Longrightarrow$", (x-s/2,y));
}

void rect(real x, real y, real a, real b)
{
	filldraw((x,y)--(x+a,y)--(x+a,y-b)--(x,y-b)--cycle,white,thin);
}

real x=0;
real y=0;


rect(x-5,y,5,7);
label("\bf Raspberry",(x-2.5,y-0.5),fontsize(12pt));
label("{\sc(uart tx)} {\sc gipo} 14",(x,y-1.5),W);
label("{\sc(uart rx)} {\sc gipo} 15",(x,y-2),W);
label("{(WiringPi 21)} {\sc gipo} 5",(x,y-4),W);
label("{(WiringPi 22)} {\sc gipo} 6",(x,y-4.5),W);
label("{(WiringPi 23)} {\sc gipo} 13",(x,y-5),W);
label("{(WiringPi 25)} {\sc gipo} 26",(x,y-6),W);

void shift(real x, real y)
{
	rect(x,y,3,5.5);
	label("\bf 74HC595",(x+1.5,y-0.5),fontsize(12pt));
	for(int i=0;i<8;++i)
	{
		label(string(i),(x+3,y-1.5-0.5*i),W);
	}
	label("SH\_CP",(x,y-1.5),E);
	label("ST\_CP",(x,y-2),E);
	label("DS",(x,y-2.5),E);
	label("Q7'",(x,y-3.5),E);
	label("MR",(x,y-4.5),E);
	label("OE",(x,y-5),E);

	draw((x-0.5,y-4.5)--(x,y-4.5));
	draw((x-0.5,y-5)--(x,y-5));
	label("$+$",(x-0.5,y-4.5),W);
	label("$-$",(x-0.5,y-5),W);
}
shift(x+2,y-2.5);
for(int i=0;i<5;++i) if(i!=3)
draw((x,y-4-0.5*i)--(x+2,y-4-0.5*i),thin);

draw((x,y-1.5)--(x+4.5,y-1.5)--(x+4.5,y-1.5+0.1)--(x+8.5,y-1.5+0.1)--(x+8.5,y-8.5+0.1),thin);
draw((x,y-2)--(x+1,y-2)--(x+1,y-.5)--(x+14.5,y-.5)--(x+14.5,y-8.5),thin);
hneg(x+3+0.5,y-1.5);
hrneg(x+3,y-0.5);
d(x+4,y-0.5);
draw((x+4,y-0.5)--(x+4,y)--(x+6,y),thin);
draw("$-$",(x+6,y),E);
rect(x+4.5,y+0.1,1,0.2);

x+=5;
y-=1.5;

void ledbox(real x, real y)
{
	rect(x,y+0.4,2,0.8);
	led(x+0.6,y);
	led(x+1.2,y);
	led(x+1.8,y);
}

real x0 = x;
for(int i=0;i<8;++i)
{
	x=x0;
	int j=i;
	if(i>=5) j=7-i+2;
	if(i<7) d(x+3.5,y-i+0.1);
	draw((x+3.5,y-i+0.1) -- (x+4,y-i+0.1),thin);
	draw((x+4.5,y-i) -- (x+5.5,y-i),thin);
	draw((x,y-2.5-0.5*i)--(x+0.5+j*0.5,y-2.5-0.5*i)--(x+0.5+j*0.5,y-i-0.1) -- (x+4,y-i-0.1),thin);
	hand(x+4.5,y-i);
	x+=5.5;
	ledbox(x,y-i);
	if(i==2 || i==3 || i==5) continue;
	x+=2;
	draw((x,y-i) -- (x+2,y-i),thin);
	if(i<7) d(x+2,y-i);

	diod(x+1.3,y-i);
}
