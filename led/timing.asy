import graph;
import settings;
import patterns;
outformat="pdf";

//size(4mm*16,66mm);
unitsize(50);
pen thin = black + roundcap + 0.4pt;
pen minpen = black + roundcap + 0.15pt + fontsize(7pt);
pen dotpen = black + roundcap + 3pt;
defaultpen(fontsize(10pt));

real y0=0;
real y1=0.5;
real dy=0.8;
real dyr=0.1;
real ty=-0.5;

real x=0;
real z=2;
draw((x,dy-dyr)--(x,dy+dyr),thin);
label("\def\ms{\,{\rm \mu s}}$0.4 \ms$",(x+0.4*z/2,ty));
label("\def\ms{\,{\rm \mu s}}$0.85 \ms$",(x+(0.4+0.85/2)*z,ty));
label("0",(x+1.25*z/2,dy));
draw((x,y0)--(x,y1)--(x+0.4*z,y1)--(x+0.4*z,y0)--(x+1.25*z,y0),thin);
x+=1.25*z;
draw((x,dy-dyr)--(x,dy+dyr),thin);
label("\def\ms{\,{\rm \mu s}}$0.8 \ms$",(x+0.8*z/2,ty));
label("\def\ms{\,{\rm \mu s}}$0.45 \ms$",(x+(0.8+0.45/2)*z,ty));
label("1",(x+1.25*z/2,dy));
draw((x,y0)--(x,y1)--(x+0.8*z,y1)--(x+0.8*z,y0)--(x+1.25*z,y0),thin);
x+=1.25*z;
draw((x,dy-dyr)--(x,dy+dyr),thin);
label("Konec signálu",(x+2*z/2,dy));
draw((x,y0)--(x+1.2*z,y0),thin);
label("\def\ms{\,{\rm \mu s}}$\ge 50 \ms$",(x+1*z,ty));
label("…",(x+3*z/2,y0));

